Django==2.0.6
dj-database-url==0.5.0
gunicorn==19.8.1
psycopg2-binary==2.7.5
SQLAlchemy==1.2.10
whitenoise==3.3.1
