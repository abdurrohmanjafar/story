from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect

# Create your views here.
def home(request):
    return render(request,'pages/index.html',{})
def story4home(request):
    url = 'https://story-jafarabdurrohman.herokuapp.com/'
    return HttpResponseRedirect(url)
def story4lainnya(request):
    url = 'https://story-jafarabdurrohman.herokuapp.com/lain'
    return HttpResponseRedirect(url)