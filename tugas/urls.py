from django.urls import path
from . import views
app_name = 'main'
urlpatterns = [
    path('',views.home,name='index'),
    path('home', views.story4home, name='story4home'),
    path('lainnya', views.story4lainnya, name='story4lainnya'),
]